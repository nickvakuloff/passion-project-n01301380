﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WWiki.Models
{
    public class Faction
    {
        [Key]
        public int FactionID { get; set; }

        [Required, StringLength(255), Display(Name = "Faction Name")]
        public string FactionName { get; set; }
        
        [Required, StringLength(10000), Display(Name = "Faction Bio")]
        public string FactionBio { get; set; }

        //profile picture
        //1=>picture exists (in img/faction/{id}.img)
        //0=>picture doesn't exist
        public int HasPic { get; set; }

        //Accepted image formats (jpg/jpeg/png/gif)
        public string ImgType { get; set; }

        public virtual ICollection<Race> Races { get; set; }
    }
}