﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace WWiki.Models
{
    public class CharacterCMSContext : DbContext
    {
    public CharacterCMSContext()
        {

        }

    public DbSet<Character> Characters { get; set; }
    public DbSet<Faction> Factions { get; set; }
    public DbSet<Race> Races { get; set; }


    }

}