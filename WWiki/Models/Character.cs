﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WWiki.Models
{
    public class Character
    {
        [Key]
        public int CharacterID { get; set; }

        [Required, StringLength(255), Display(Name = "Character Name")]
        public string CharacterName { get; set; }

        [Required, Display(Name = "Character Race")]
        public string CharacterRace { get; set; }

        [Required, Display(Name = "Character Faction")]
        public string CharacterFaction { get; set; }

        [Required, StringLength(10000), Display(Name = "Character Bio")]
        public string CharacterBio { get; set; }

        //profile picture
        //1=>picture exists (in img/character/{id}.img)
        //0=>picture doesn't exist
        public int HasPic { get; set; }
        
        //Accepted image formats (jpg/jpeg/png/gif)
        public string ImgType { get; set; }

        public virtual Race race { get; set; }

    }
}