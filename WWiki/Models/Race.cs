﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WWiki.Models
{
    public class Race
    {
        [Key]
        public int RaceID { get; set; }

        [Required, StringLength(255), Display(Name = "Race Name")]
        public string RaceName { get; set; }

        //[ForeignKey("FactionID")]
        //public int RaceFaction { get; set; }
        //public virtual Faction Faction { get; set; }

        [Required, StringLength(255), Display(Name = "Race Faction")]
        public string RaceFaction { get; set; }

        [Required, StringLength(10000), Display(Name = "Race Bio")]
        public string RaceBio { get; set; }

        //profile picture
        //1=>picture exists (in img/faction/{id}.img)
        //0=>picture doesn't exist
        public int HasPic { get; set; }

        //Accepted image formats (jpg/jpeg/png/gif)
        public string ImgType { get; set; }

        public virtual ICollection<Character> Characters { get; set; }
    }
}