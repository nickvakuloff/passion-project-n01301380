﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Data.Entity;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WWiki.Models;
using WWiki.Models.ViewModels;
using System.Diagnostics;
using System.IO;

namespace WWiki.Controllers
{
    public class RaceController : Controller
    {
        CharacterCMSContext db = new CharacterCMSContext();
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }
        public ActionResult List()
        {
            string query = "select * from races";
            IEnumerable<Race> races = db.Races.SqlQuery(query);

            return View(races);
        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(string RaceName_New, string RaceFaction_New, string RaceBio_New)
        {
            string query = "insert into races (RaceName, RaceFaction, RaceBio, HasPic)" + " values (@rname, @rfaction, @rbio, 0)";
            SqlParameter[] myparams = new SqlParameter[3];
            myparams[0] = new SqlParameter("@rname", RaceName_New);
            myparams[1] = new SqlParameter("@rfaction", RaceFaction_New);
            myparams[2] = new SqlParameter("@rbio", RaceBio_New);

            db.Database.ExecuteSqlCommand(query, myparams);

            //Debug.WriteLine(FactionName_New + " " + FactionBio_New);
            //Debug.WriteLine(query);

            return RedirectToAction("List");
        }

        public ActionResult Edit(int? id)
        {
            if ((id == null) || (db.Races.Find(id) == null))
            {
                return HttpNotFound();
            }
            string query = "select * from races where raceid=@id";
            SqlParameter param = new SqlParameter("@id", id);
            Race myrace = db.Races.SqlQuery(query, param).FirstOrDefault();
            return View(myrace);
        }

        [HttpPost]
        public ActionResult Edit(int? id, string RaceName, string RaceFaction, string RaceBio)
        {
            if ((id == null) || (db.Races.Find(id) == null))
            {
                return HttpNotFound();
            }
            string query = "update races set racename = @rname, racefaction = @rfaction, racebio = @rbio where raceid=@id";
            SqlParameter[] myparams = new SqlParameter[4];
            myparams[0] = new SqlParameter("@rname", RaceName);
            myparams[1] = new SqlParameter("@rfaction", RaceFaction);
            myparams[2] = new SqlParameter("@rbio", RaceBio);
            myparams[3] = new SqlParameter("@id", id);

            db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("Show/" + id);
        }

        // GET: Authors/Edit/5



        //v2.0
        //public ActionResult Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Faction faction = db.Factions.Find(id);
        //    if (faction == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(faction);
        //}

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "FactionID,FactionName, FactionBio")] Faction faction, HttpPostedFileBase factionimg)
        //{
        //    faction.HasPic = 0;

        //    if(factionimg.ContentLength > 0)
        //    {
        //        var valtypes = new[] { "jpeg", "jpg", "png", "gif" };
        //        var extension = Path.GetExtension(factionimg.FileName).Substring(1);

        //        if (valtypes.Contains(extension))
        //        {
        //            string fn = faction.FactionID + "." + extension;
        //            string path = Path.Combine(Server.MapPath("~/img/factions"), fn);
        //            factionimg.SaveAs(path);
        //            faction.HasPic = 1;
        //            faction.ImgType = extension;
        //            Debug.WriteLine(fn);
        //            Debug.WriteLine(path);

        //        }

        //    }
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(faction).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("List");
        //    }
        //    return View(faction);
        //}

        //v3.0
        //public ActionResult Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Faction faction = db.Factions.Find(id);
        //    if (faction == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(faction);
        //}
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "FactionID,FactionName,FactionBio")] Faction faction, HttpPostedFileBase factionimg)
        //{
        //    //default to false (no pic)
        //    faction.HasPic = 0;


        //    if(factionimg != null)
        //    {
        //        if (factionimg.ContentLength > 0)
        //        {
        //            //file extensioncheck taken from https://www.c-sharpcorner.com/article/file-upload-extension-validation-in-asp-net-mvc-and-javascript/
        //            var valtypes = new[] { "jpeg", "jpg", "png", "gif" };
        //            var extension = Path.GetExtension(factionimg.FileName).Substring(1);

        //            if (valtypes.Contains(extension))
        //            {

        //                //generic .img extension, web translates easily.
        //                string fn = faction.FactionID + "." + extension;

        //                //get a direct file path to imgs/authors/
        //                string path = Path.Combine(Server.MapPath("~/img/factions"), fn);

        //                //save the file
        //                factionimg.SaveAs(path);

        //                //let the model know that there is a picture with an extension
        //                faction.HasPic = 1;
        //                faction.ImgType = extension;

        //            }
        //        }
        //    }
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(faction).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("List");
        //    }
        //    return View(faction);
        //}

        public ActionResult Delete(int? id)
        {
            if ((id == null) || (db.Races.Find(id) == null))
            {
                return HttpNotFound();
            }
            string query = "delete from Races where raceid = @id";
            SqlParameter param = new SqlParameter("@id", id);
            db.Database.ExecuteSqlCommand(query, param);

            // Debug.WriteLine(param);
            //Debug.WriteLine(query);

            return RedirectToAction("List");
        }

        public ActionResult Show(int? id)
        {
            if ((id == null) || (db.Races.Find(id) == null))
            {
                return HttpNotFound();
            }

            string query = "select * from races where raceid = @id";

            SqlParameter param = new SqlParameter("@id", id);

            Race racetoshow = db.Races.Find(id);

            return View(racetoshow);
        }


    }
}